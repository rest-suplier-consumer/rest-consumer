package com.gol.consumer.controller;

import com.gol.consumer.domain.TestFormDTO;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j
@Controller
public class WelcomeController {

    @GetMapping("/")
    public String welcomePage(final Model model) {

        model.addAttribute("welcomeText","Hello World");

        return "welcome";
    }

    @GetMapping("/form")
    public String formTest(final Model model) {
        model.addAttribute("testFormDTO", new TestFormDTO());
        return "form";
    }

//    @PostMapping("/form")
//    public String formTest(@ModelAttribute("testFormDTO") final TestFormDTO testFormDTO, @RequestParam("attachment") MultipartFile attachment)
//    {
//        log.debug(String.format("%s - %s", "testFormDTO", testFormDTO.toString()));
//        log.debug(String.format("%s - %s", "attachment", attachment));
//        return "redirect:/form";
//    }

    @PostMapping("/form")
    public String formTest(@ModelAttribute("testFormDTO") final TestFormDTO testFormDTO) throws IOException {
        log.debug(String.format("%s - %s", "testFormDTO", testFormDTO.toString()));
        log.debug(String.format("%s - name: %s, content-type: %s, original-filename: %s, size: %s",
                "attachment",
                testFormDTO.getAttachment().getName(),
                testFormDTO.getAttachment().getContentType(),
                testFormDTO.getAttachment().getOriginalFilename(),
                testFormDTO.getAttachment().getSize()));
        log.debug(testFormDTO.getAttachment().getBytes().toString());
        return "redirect:/form";
    }

}
