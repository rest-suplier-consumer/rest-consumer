package com.gol.consumer.service.impl;

import com.gol.consumer.domain.City;
import com.gol.consumer.domain.CityListDTO;
import com.gol.consumer.service.CityService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
public class CityServiceSuplierConnectionImpl implements CityService {

    private RestTemplate restTemplate = new RestTemplate();
    private List<City> cities = new ArrayList<>();

    @Scheduled(fixedRate = 10000)
    public void getCitiesFromResource() {

        log.debug("getCitiesFromResource is running in schedule!");

        String resourceUrl = "http://localhost:8081/cityListObject";
        Optional<CityListDTO> optionalCityListDTO = Optional.empty();

        try {
            optionalCityListDTO = Optional.ofNullable(restTemplate.getForObject(resourceUrl, CityListDTO.class));
        } catch (ResourceAccessException ex) {
            log.debug(ex.getClass());
            log.debug(ex.getMessage());
            log.debug(ex.getLocalizedMessage());
        }
        catch (HttpClientErrorException ex) {
            log.debug(ex.getClass());
            log.debug(ex.getMessage());
            log.debug(ex.getLocalizedMessage());
            log.debug(ex.getRawStatusCode());
            log.debug(ex.getResponseHeaders());
            log.debug(ex.getResponseBodyAsString());
        }
        catch (Exception ex) {

            log.debug(ex.getClass());
            log.debug(ex.getMessage());
            log.debug(ex.getLocalizedMessage());
        }

        optionalCityListDTO.ifPresent(cityDTO -> this.cities = cityDTO.getCities());
    }

    @Override
    public List<City> getAllCities() {

        return cities;
    }
}
