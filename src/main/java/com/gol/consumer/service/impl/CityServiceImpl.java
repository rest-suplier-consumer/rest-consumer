package com.gol.consumer.service.impl;

import com.gol.consumer.domain.City;
import com.gol.consumer.repository.CityRepository;
import com.gol.consumer.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;

    @Override
    public List<City> getAllCities() {
        List<City> cityList = cityRepository.findAll();
        return cityList;
    }
}
