package com.gol.consumer.service;

import com.gol.consumer.domain.City;

import java.util.List;

public interface CityService {

    List<City> getAllCities();

}
