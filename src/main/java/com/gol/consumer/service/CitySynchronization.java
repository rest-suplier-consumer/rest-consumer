package com.gol.consumer.service;

import com.gol.consumer.domain.City;
import com.gol.consumer.domain.CityListDTO;
import com.gol.consumer.repository.CityRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Log4j
@Service
public class CitySynchronization {

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    CityRepository cityRepository;


    @Scheduled(fixedRate = 30000)
    public void getCitiesFromResource() {

        log.debug("CitySynchronization service is running in schedule!");

        final String resourceUrl = "http://localhost:8081/cityListObject";
        Optional<CityListDTO> optionalCityListDTO = Optional.empty();

        try {
            optionalCityListDTO = Optional.ofNullable(restTemplate.getForObject(resourceUrl, CityListDTO.class));
        }
        catch (Exception ex) {
            log.error(String.format("%s - %s: %s", "CitySynchronization error", ex.getClass(),ex.getMessage()));
        }

        optionalCityListDTO.ifPresent(cityListDTO -> updateDatabase(cityListDTO.getCities()));

    }

    @Transactional
    private void updateDatabase(List<City> cities) {

        final Map<String, City> cityCodeMap = cities
                .stream()
                .collect(Collectors.toMap(City::getCode, Function.identity()));

        final List<City> persistedCities= cityRepository.findAll();
        final List<City> nonExistentCities = new ArrayList<>();

        persistedCities.forEach(city -> {
            String key = city.getCode();
            if( cityCodeMap.containsKey(key) ){
                cityCodeMap.get(key).setId( city.getId() );
            } else {
                nonExistentCities.add(city);
            }
        });

        cityRepository.save(cities);
        cityRepository.delete(nonExistentCities);

    }

}
