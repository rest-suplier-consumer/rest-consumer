package com.gol.consumer.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class TestFormDTO {

    private String firstName;
    private String lastName;
    private MultipartFile attachment;

    @Override
    public String toString() {
        return "TestFormDTO{" +
                "firtName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
