package com.gol.consumer.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Entity
@Table (name = "city",
        indexes = {
                @Index(name = "city_name", columnList = "name")
        })
public class City implements Serializable {


    private static final long serialVersionUID = -2684219251765686714L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(length = 10, nullable = false, unique = true)
    private String code;

    @NotNull
    @Column(length = 100, nullable = false)
    private String name;

    @NotNull
    private BigInteger residentsNumber;

}
